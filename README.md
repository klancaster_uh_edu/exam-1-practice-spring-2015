# CIS 4339 Practice Exam #


Clone this repository and work through the requirements in the file called "example-questions.pdf" to prepare for the exam.

When you have finished working through the questions, you can change to the solution branch to see the completed app. Note that the completed app includes the bonus questions as well. It is *left as an exercise* to for you to figure out how to change branches :-). You will NOT have to change branches on the exam.